package com.example.simplerestapis.controller;
import com.example.simplerestapis.models.MpesaIntermediateResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.simplerestapis.models.PostRequest;
import com.example.simplerestapis.models.PostResponse;
import com.example.simplerestapis.models.SampleResponse;

@RestController
public class WebController {

	@RequestMapping("/mpesabroker")
	public SampleResponse Sample(@RequestParam(value = "originator",
	defaultValue = "Robot") String name) {
		SampleResponse response = new SampleResponse();
		response.setId(1);
		response.setMessage("Your name is "+name);
		return response;

	}
	
	@RequestMapping(value = "/mpesabroker", method = RequestMethod.POST)
	public PostResponse Test(@RequestBody PostRequest inputPayload) {
		PostResponse response = new PostResponse();
		response.setId(inputPayload.getId()*100);
		response.setMessage("Hello " + inputPayload.getName());
		response.setExtra("Some text");
		return response;
	}
        
        /**
         * Process Command ID
         * 
         * 
         * @return JSON for sync, XML for Async responses to Command IDs
         */
        public Object processCommandId(String commandID) {
            String originatorConvID = "";
            String mpesaTransactionId = "";
            MpesaIntermediateResponse intermediateResponse = new MpesaIntermediateResponse();
                    
            switch (commandID) {
                case "AccountBalance":
                    
                    break;
                case "BusinessPayment":
                    // completable future for async request.
                    // return sync.
                    break;
                    
                case "BusinessPaybill":
                    // completable future for async request.
                    // return sync.
                    break;
                
                case "TransactionStatusQuery":
                    // completable future for async request.
                    // return sync.
                    break;
                    
            }
            
            // Run thread for Aysn Response
            
            return intermediateResponse;
        }
}